﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment
{
    public partial class Form1 : Form
    {
        /* 
 * @(#)fractal.java - Fractal Program Mandelbrot Set 
 * www.eckhard-roessel.de
 * Copyright (c) Eckhard Roessel. All Rights Reserved.
 * 06/30/2000 - 03/29/2000
 */
        //import java.awt.*;
        //import java.applet.*;
        //import java.awt.event.*;
        /** 
         * @version 1.1
         * @author Eckhard Roessel
         * @modified 03/29/2001
         * @modified 08/16/2006 tweaked by Duncan Mullier 8/2006
         * 
         * @version 4.0
         * @modified 07/11/2014 Tom Knowles(TK)
         */
        //public class Fractal extends Applet implements MouseListener, MouseMotionListener
        public Form1()
        {
            InitializeComponent();
            init(); //added 31/10 TK
            start(); //added 31/10 TK 
        }

        class HSB
        {//djm added, it makes it simpler to have this code in here than in the C#
            public float rChan, gChan, bChan;
            public HSB()
            {
                rChan = gChan = bChan = 0;
            }
            public void fromHSB(float h, float s, float b)
            {
                float red = b;
                float green = b;
                float blue = b;
                if (s != 0)
                {
                    float max = b;
                    float dif = b * s / 255f;
                    float min = b - dif;

                    float h2 = h * 360f / 255f;

                    if (h2 < 60f)
                    {
                        red = max;
                        green = h2 * dif / 60f + min;
                        blue = min;
                    }
                    else if (h2 < 120f)
                    {
                        red = -(h2 - 120f) * dif / 60f + min;
                        green = max;
                        blue = min;
                    }
                    else if (h2 < 180f)
                    {
                        red = min;
                        green = max;
                        blue = (h2 - 120f) * dif / 60f + min;
                    }
                    else if (h2 < 240f)
                    {
                        red = min;
                        green = -(h2 - 240f) * dif / 60f + min;
                        blue = max;
                    }
                    else if (h2 < 300f)
                    {
                        red = (h2 - 240f) * dif / 60f + min;
                        green = min;
                        blue = max;
                    }
                    else if (h2 <= 360f)
                    {
                        red = max;
                        green = min;
                        blue = -(h2 - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        red = 0;
                        green = 0;
                        blue = 0;
                    }
                }

                rChan = (float)Math.Round(Math.Min(Math.Max(red, 0), 255)); //Changed round to Round, also added (float), changed max to Max & min to Min(Tk_17/10)
                gChan = (float)Math.Round(Math.Min(Math.Max(green, 0), 255));//Changed round to Round, also added (float), changed max to Max & min to Min(Tk_17/10)
                bChan = (float)Math.Round(Math.Min(Math.Max(blue, 0), 255)); //Changed round to Round, also added (float), changed max to Max & min to Min(Tk_17/10)

            }
        }



        private const int MAX = 256;      // max iterations //changed final to const(Tk_17/10)
        private const double SX = -2.025; // start value real //changed final to const(Tk_17/10)
        private const double SY = -1.125; // start value imaginary //changed final to const(Tk_17/10)
        private const double EX = 0.6;    // end value real //changed final to const(Tk_17/10)
        private const double EY = 1.125;  // end value imaginary //changed final to const(Tk_17/10)
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished; //changed boolean to bool(Tk_17/10)
        private static float xy;
        //private PictureBox picture;
        private Bitmap picture; //changed from Image to Bitmap (TK_24/10)
        private Graphics g1;
        private Cursor c1, c2;
        private HSB HSBcol = new HSB();
        Pen Pen1 = new Pen(Color.Blue, 1);



        public void init() // all instances will be prepared
        {
            //HSBcol = new HSB();

            //changed in the visual editor (TK_24/10)
            finished = false;
            //addMouseListener(this);
            //addMouseMotionListener(this);
            //c1 = Cursors.WaitCursor;//changed to Cusors.WaitCursor(TK_24/10)
            //c2 = Cursors.Cross;//changed to Cursors.Cross(TK_24/10)
            this.ClientSize = new System.Drawing.Size(640, 480);
            x1 = pictureBox1.Width;
            y1 = pictureBox1.Height;
            xy = (float)x1 / (float)y1;
            //pictureBox1 = new PictureBox();
            picture = new Bitmap(x1, y1); //changed from picture = CreateGraphics(x1, y1);  to = new Bitmap(x1,y1); (TK_24/10) 
            g1 = Graphics.FromImage(picture); //changed from picture.getGraphics to Graphics.FromImage(picture); (TK_24/10)
            finished = true;
        }

        public void destroy() // delete all instances 
        {
            if (finished)
            {
                //removeMouseListener(this);
                //removeMouseMotionListener(this);
                picture = null;
                g1 = null;
                c1 = null;
                c2 = null;
                GC.Collect(); // garbage collection
            }
        }

        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }

        public void stop()
        {
        }

        public void paint(Graphics g)
        {
            update(g);
        }

        public void update(Graphics g)
        {
            g.DrawImage(picture, 0, 0); // changed from g.drawImage(picture, 0, 0, this); (TK_24/10)
            if (rectangle)
            {
                Pen MyPen = new Pen(Color.White); //changed from g.setColor(Color.white); (TK_24/10)

                if (xs < xe)
                {
                    if (ys < ye) g.DrawRectangle(MyPen, xs, ys, (xe - xs), (ye - ys)); //changed from g.drawRect(xs, ys, (xe - xs), (ye - ys)); (TK_28/10)
                    else g.DrawRectangle(MyPen, xs, ye, (xe - xs), (ys - ye)); //changed from g.drawRect(xs, ye, (xe - xs), (ys - ye)); (TK_28/10)
                }
                else
                {
                    if (ys < ye) g.DrawRectangle(MyPen, xe, ys, (xs - xe), (ye - ys)); //changed from g.drawRect(xe, ys, (xs - xe), (ye - ys)); (TK_28/10)
                    else g.DrawRectangle(MyPen, xe, ye, (xs - xe), (ys - ye)); //changed from g.drawRect(xe, ye, (xs - xe), (ys - ye)); (TK_28/10)
                }
            }
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;

            action = false;
            this.Cursor = Cursors.WaitCursor;//SetCursor(c1);//changed from setCursor(c1); (TK_28/10)
            //showStatus("Mandelbrot-Set will be produced - please wait...");
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                        //djm added
                        //HSBcol.fromHSB(h,0.8f,b); //convert hsb to rgb then make a Java Color
                        //Color col = new Color(0,HSBcol.rChan,HSBcol.gChan,HSBcol.bChan);
                        //g1.setColor(col);
                        //djm end
                        //djm added to convert to RGB from HSB

                        HSBcol.fromHSB(h*255,0.8f*255, b*255);
                        Color col = Color.FromArgb((int)HSBcol.rChan, (int)HSBcol.gChan, (int)HSBcol.bChan);
                        Pen1.Color = col;

                        /*g1.setColor(Color.getHSBColor(h, 0.8f, b));
                        //djm test
                    Color col = Color.getHSBColor(h,0.8f,b);
                    int red = col.getRed();
                    int green = col.getGreen();
                    int blue = col.getBlue();*/
                        //djm 
                        alt = h;
                    }
                    g1.DrawLine(Pen1, x, y, x + 1, y); //changed from drawLine to DrawLine (TK 30/10)
                }
            //showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
            this.Cursor = Cursors.Cross; //SetCursor(c2);
            action = true;
        }


        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }


        private void pictureBox1_MouseClick(object sender, MouseEventArgs e) //replaces Form1 mouseclick  //changed from public void mouseClicked(object sender, MouseEventArgs e) (TK_31/10)
        {

        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e) //replaces Form1 mousedown //changed from public void mousePressed(object sender, MouseEventArgs e) (TK_31/10)
        {
            //e.consume(); edited out to compile 

            if (action)
            {
                xs = e.X;
                ys = e.Y;
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e) //replaces Form1 version of mouse moved. //changed from public void mouseDragged(object sender, MouseEventArgs e) (TK_31/10)
        {

            //e.consume();edited out to compile (TK_29/10)

            if (e.Button == MouseButtons.Left)
            {

                if (action)
                {
                    xe = e.X;
                    ye = e.Y;
                    rectangle = true;
                    //repaint();
                    this.Refresh();
                }
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e) //replaces Form1 version of Mouse Up. //changed from  public void mouseReleased(object sender, MouseEventArgs e) (TK_31/10)
        {
            int z, w;

            //e.consume(); edited out to compile (TK_30/10)
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                //repaint();
                this.Refresh();
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e) // replaced the Form1 version of Paint which has now been deleted.
        {
            paint(e.Graphics);
        }

                public String getAppletInfo()
        {
            return "fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001";
        }

                private void pictureBox1_Click(object sender, EventArgs e)
                {

                }

                private void button1_Click(object sender, EventArgs e) //added 7/11  by TK, this allows the user to save the fractal as Jpeg, bitmap or Gif file.
                {
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
                    saveFileDialog1.Title = "Save an Image File";
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        picture.Save(saveFileDialog1.FileName);
                    }

                }

                private void button2_Click(object sender, EventArgs e)//08/11 exports the information needed to reload up an image of the fractal
                {
                    SaveFileDialog saveFileDialog2 = new SaveFileDialog();
                    saveFileDialog2.Filter = "Text Files (*.txt)|*.txt";
                    saveFileDialog2.Title = "Save a Text File";

                    if (saveFileDialog2.ShowDialog() == DialogResult.OK)
                    {
                        //09/11_TK the lines below write the values of each int and double below in a text file which is then saved. 
                        StreamWriter writer = new StreamWriter(saveFileDialog2.FileName);
                        writer.WriteLine(x1);
                        writer.WriteLine(y1);
                        writer.WriteLine(xs);
                        writer.WriteLine(ys);
                        writer.WriteLine(xe);
                        writer.WriteLine(ye);
                        writer.WriteLine(xstart);
                        writer.WriteLine(ystart);
                        writer.WriteLine(xende);
                        writer.WriteLine(yende);
                        writer.WriteLine(xzoom);
                        writer.WriteLine(yzoom);
                        writer.Close();
                        
                    }

                }

                private void button3_Click(object sender, EventArgs e)//09/11 added to import zoomed image of fractal to carry on. 
                {
                    OpenFileDialog openFileDialog1 = new OpenFileDialog();
                    
                    openFileDialog1.Title = "Open Text File";
                    openFileDialog1.Filter ="Text Files (*.txt)|*.txt";
                    openFileDialog1.InitialDirectory = @"C:\";

                    
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        StreamReader reader = new StreamReader(openFileDialog1.FileName);
                        //09/11_TK This reads all of the lines in the saved text file 
                        string x1a = reader.ReadLine();
                        string y1a = reader.ReadLine();
                        string xs1 = reader.ReadLine();
                        string ys1 = reader.ReadLine();
                        string xe1 = reader.ReadLine();
                        string ye1 = reader.ReadLine();
                        string xstart1 = reader.ReadLine();
                        string ystart1 = reader.ReadLine();
                        string xende1 = reader.ReadLine();
                        string yende1 = reader.ReadLine();
                        string xzooma = reader.ReadLine();
                        string yzooma = reader.ReadLine();
                        reader.Close();

                        //09/11_TK this converts the strings which were in the text file to int and doubles meaning them can be used
                        int x1 = Convert.ToInt32(x1a);
                        int y1 = Convert.ToInt32(y1a);
                        int xs = Convert.ToInt32(xs1);
                        int ys = Convert.ToInt32(ys1);
                        int xe = Convert.ToInt32(xe1);
                        int ye = Convert.ToInt32(ye1);
                        double xstart = Convert.ToDouble(xstart1);
                        double ystart = Convert.ToDouble(ystart1);
                        double xende = Convert.ToDouble(xende1);
                        double yende = Convert.ToDouble(yende1);
                        double xzoom = Convert.ToDouble(xzooma);
                        double yzoom = Convert.ToDouble(yzooma);
                        
                    }

                    

                }

    }
}
